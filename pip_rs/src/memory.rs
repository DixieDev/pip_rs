use crate::{Capsule, Circle, Rigidbody, OrientedBox};
use crate::allocator::{Allocator, AllocErr, FreeErr};
use crate::maths;
use std::marker::PhantomData;
use std::mem::size_of;

#[derive(Clone)]
pub struct RbIter<Real: maths::Real, Rb: Rigidbody<Real>> {
    start: *mut DataContainer<Real, Rb>,
    current: usize,
    length: usize,
}

impl <Real: maths::Real, Rb: Rigidbody<Real>> RbIter<Real, Rb> {
    fn new(start: *mut DataContainer<Real, Rb>, length: usize) -> Self {
        Self {
            start, 
            length,
            current: 0,
        }
    }
}

impl <Real: maths::Real, Rb: Rigidbody<Real>> Iterator for RbIter<Real, Rb> {
    type Item = *mut Rb;

    fn next(&mut self) -> Option<Self::Item> {
        let result = if self.current < self.length {
            let data_ptr = unsafe {
                let container_ptr = self.start.add(self.current);
                (&mut (*container_ptr).data) as *mut Rb
            };

            Some(data_ptr)
        } else {
            None 
        };

        self.current += 1;
        result
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum CreationErr {
    OutOfSlots {
        capacity: usize,
    },
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum DerefErr {
    OutOfBounds {
        capacity: usize,
        offset: usize,
    },
    InactiveMapping,
    InvalidHandleGeneration {
        handle_gen: u64,
        mapping_gen: u64,
    },
}

#[derive(Clone, Copy, Eq, PartialEq)] pub struct MappingIdx(usize);
#[derive(Clone, Copy, Eq, PartialEq)] pub struct DataIdx(usize);

#[derive(Clone, Copy, Eq, PartialEq)]
struct Handle {
    gen: u64,
    idx: MappingIdx,
}

#[derive(Clone, Copy)] pub struct CircleHandle(Handle);
#[derive(Clone, Copy)] pub struct CapsuleHandle(Handle);
#[derive(Clone, Copy)] pub struct OrientedBoxHandle(Handle);

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Mapping {
    active: bool,
    gen: u64,
    idx: DataIdx,
}

#[derive(Clone)]
pub struct DataContainer<Real: maths::Real, Rb: Rigidbody<Real>> {
    mapping_idx: MappingIdx,
    data: Rb,
    _phantom: PhantomData<Real>,
}

impl<Real: maths::Real, Rb: Rigidbody<Real>> DataContainer<Real, Rb> {
    pub fn new(mapping_idx: MappingIdx, data: Rb) -> Self {
        Self {
            mapping_idx,
            data,
            _phantom: PhantomData
        }
    }
}

pub struct MemoryConfig {
    pub num_circles: usize,
    pub num_capsules: usize,
    pub num_boxes: usize,
}

pub struct Partition<Real: maths::Real, Rb: Rigidbody<Real>> {
    mapping_start: *mut Mapping,
    data_start: *mut DataContainer<Real, Rb>,
    capacity: usize,
    length: usize,
    _phantom: PhantomData<Real>,
}

impl<Real: maths::Real, Rb: Rigidbody<Real>> Partition<Real, Rb> {
    pub fn new(mapping_start: *mut Mapping, data_start: *mut DataContainer<Real, Rb>, capacity: usize) -> Self {
        Self {
            mapping_start,
            data_start,
            capacity,
            length: 0,
            _phantom: PhantomData,
        }
    }
}

pub struct Memory<Real: maths::Real> {
    start: *mut u8,
    circles: Partition<Real, Circle<Real>>,
    capsules: Partition<Real, Capsule<Real>>,
    boxes: Partition<Real, OrientedBox<Real>>,
}

impl<Real: maths::Real> Memory<Real> {
    pub unsafe fn new<A: Allocator>(allocator: &mut A, config: MemoryConfig) -> Result<Self, AllocErr> {
        // Ignores alignement for now, since it's a pain and the gains are dubious on modern hardware

        let MemoryConfig { num_circles, num_capsules, num_boxes } = config;

        let mapping_size = size_of::<Mapping>();
        let circle_size = size_of::<DataContainer<Real, Circle<Real>>>();
        let capsule_size = size_of::<DataContainer<Real, Capsule<Real>>>();
        let box_size = size_of::<DataContainer<Real, OrientedBox<Real>>>();

        // Determine ranges of each memory view
        let circle_mapping_start = 0;
        let circle_mapping_end = circle_mapping_start + num_circles * mapping_size;
        let circle_start = circle_mapping_end;
        let circle_end = circle_start + num_circles * circle_size;

        let capsule_mapping_start = circle_end;
        let capsule_mapping_end = capsule_mapping_start + num_capsules * mapping_size;
        let capsule_start = capsule_mapping_end;
        let capsule_end = capsule_start + num_capsules * capsule_size;

        let box_mapping_start = capsule_end;
        let box_mapping_end = box_mapping_start + num_boxes * mapping_size;
        let box_start = box_mapping_end;
        let box_end = box_start + num_boxes * box_size;

        // Allocate the mem we need
        let required_mem = box_end;
        let start = allocator.alloc(required_mem)?;

        // Set all mappings to be inactive with generation set to 0
        let pairs = [
            (circle_mapping_start, num_circles),
            (capsule_mapping_start, num_capsules),
            (box_mapping_start, num_boxes),
        ];

        for (initial_offset, num_mappings) in pairs.iter().cloned() {
            let mut offset = initial_offset;
            for _ in 0..num_mappings {
                let mapping_ptr = start.add(offset) as *mut Mapping;
                (*mapping_ptr).gen = 0;
                (*mapping_ptr).active = false;
                offset += mapping_size;
            }
        }

        // Initialise views
        Ok(Self {
            start,

            circles: Partition::new(
                start.add(circle_mapping_start) as *mut Mapping,
                start.add(circle_start) as *mut DataContainer<Real, Circle<Real>>,
                num_circles,
            ),

            capsules: Partition::new(
                start.add(capsule_mapping_start) as *mut Mapping,
                start.add(capsule_start) as *mut DataContainer<Real, Capsule<Real>>,
                num_capsules,
            ),

            boxes: Partition::new(
                start.add(box_mapping_start) as *mut Mapping,
                start.add(box_start) as *mut DataContainer<Real, OrientedBox<Real>>,
                num_boxes,
            ),
        })
    }

    pub unsafe fn free<A: Allocator>(&mut self, allocator: &mut A) -> Result<(), FreeErr> {
        allocator.free(self.start)
    }

    unsafe fn get<Rb: Rigidbody<Real>>(partition: &mut Partition<Real, Rb>, handle: Handle) -> Result<*mut Rb, DerefErr> {
        if partition.length == 0 {
            Err(DerefErr::InactiveMapping)
        } else {
            let mapping_ptr = partition.mapping_start.add(handle.idx.0);
            if !(*mapping_ptr).active {
                Err(DerefErr::InactiveMapping)
            } else if (*mapping_ptr).gen != handle.gen {
                Err(DerefErr::InvalidHandleGeneration {
                    handle_gen: handle.gen,
                    mapping_gen: (*mapping_ptr).gen,
                })
            } else {
                let data_ptr = partition.data_start.add((*mapping_ptr).idx.0);
                Ok((&mut (*data_ptr).data) as *mut Rb)
            }
        }
    }

    unsafe fn create<Rb: Rigidbody<Real>>(partition: &mut Partition<Real, Rb>, rb: Rb) -> Result<Handle, CreationErr> {
        if partition.length >= partition.capacity {
            return Err(CreationErr::OutOfSlots {
                capacity: partition.capacity
            });
        }

        for idx in 0..partition.capacity {
            let mapping_ptr = partition.mapping_start.add(idx);
            if !(*mapping_ptr).active {
                (*mapping_ptr).active = true;
                (*mapping_ptr).gen += 1;
                (*mapping_ptr).idx = DataIdx(partition.length);

                let data_ptr = partition.data_start.add(partition.length);
                *data_ptr = DataContainer::new(MappingIdx(idx), rb);
                partition.length += 1;

                return Ok(Handle {
                    gen: (*mapping_ptr).gen,
                    idx: MappingIdx(idx),
                });
            }
        }

        panic!("Failed to find available mapping for new {:?}, but only {} out of {} slots are recorded as active", 
               Rb::TYPE, partition.length, partition.capacity);
    }

    unsafe fn remove<Rb: Rigidbody<Real>>(partition: &mut Partition<Real, Rb>, handle: Handle) -> Result<Rb, DerefErr> {
        let mapping_ptr = partition.mapping_start.add(handle.idx.0);
        if handle.idx.0 >= partition.capacity {
            Err(DerefErr::OutOfBounds {
                capacity: partition.capacity,
                offset: handle.idx.0,
            })
        } else if !(*mapping_ptr).active {
            Err(DerefErr::InactiveMapping)
        } else if (*mapping_ptr).gen != handle.gen {
            Err(DerefErr::InvalidHandleGeneration {
                handle_gen: handle.gen,
                mapping_gen: (*mapping_ptr).gen,
            })
        } else if partition.length > 1 {
            // Kill current mapping
            (*mapping_ptr).active = false;

            // Swap last data element to current position
            let data_ptr = partition.data_start.add((*mapping_ptr).idx.0);
            let last_ptr = partition.data_start.add(partition.length - 1);
            let removed_data = (*data_ptr).data.clone();
            *data_ptr = (*last_ptr).clone();

            // Update mapping for last element to its new position
            let last_mapping = partition.mapping_start.add((*last_ptr).mapping_idx.0);
            (*last_mapping).idx = (*mapping_ptr).idx;

            // Update length
            partition.length -= 1;

            Ok(removed_data)
        } else {
            // We have no one to swap with, so just kill current mapping, update length, and return
            // the removed rigidbody
            (*mapping_ptr).active = false;
            partition.length -= 1;

            let data_ptr = partition.data_start.add((*mapping_ptr).idx.0);
            Ok((*data_ptr).data.clone())
        }
    }

    unsafe fn update<Rb: Rigidbody<Real>>(partition: &mut Partition<Real, Rb>, handle: Handle, new: Rb) -> Result<Rb, DerefErr> {
        let mapping_ptr = partition.mapping_start.add(handle.idx.0);
        if handle.idx.0 >= partition.capacity {
            Err(DerefErr::OutOfBounds {
                capacity: partition.capacity,
                offset: handle.idx.0,
            })
        } else if !(*mapping_ptr).active {
            Err(DerefErr::InactiveMapping)
        } else if (*mapping_ptr).gen != handle.gen {
            Err(DerefErr::InvalidHandleGeneration {
                handle_gen: handle.gen,
                mapping_gen: (*mapping_ptr).gen,
            })
        } else {
            let data_ptr = partition.data_start.add((*mapping_ptr).idx.0);
            let old = (*data_ptr).data.clone();
            (*data_ptr).data = new;

            Ok(old)
        } 
    }

    pub unsafe fn circle_iter(&self) -> RbIter<Real, Circle<Real>> {
        RbIter::new(self.circles.data_start, self.circles.length)
    }

    pub unsafe fn get_circle(&mut self, handle: CircleHandle) -> Result<*mut Circle<Real>, DerefErr> {
        Self::get(&mut self.circles, handle.0)
    }

    pub unsafe fn create_circle(&mut self, circle: Circle<Real>) -> Result<CircleHandle, CreationErr> {
        let handle = Self::create(&mut self.circles, circle)?;
        Ok(CircleHandle(handle))
    }

    pub unsafe fn remove_circle(&mut self, handle: CircleHandle) -> Result<Circle<Real>, DerefErr> {
        Self::remove(&mut self.circles, handle.0)
    } 
    
    pub unsafe fn update_circle(&mut self, handle: CircleHandle, new_circle: Circle<Real>) -> Result<Circle<Real>, DerefErr> {
        Self::update(&mut self.circles, handle.0, new_circle)
    }

    pub unsafe fn capsule_iter(&self) -> RbIter<Real, Capsule<Real>> {
        RbIter::new(self.capsules.data_start, self.capsules.length)
    }

    pub unsafe fn get_capsule(&mut self, handle: CapsuleHandle) -> Result<*mut Capsule<Real>, DerefErr> {
        Self::get(&mut self.capsules, handle.0)
    }

    pub unsafe fn create_capsule(&mut self, capsule: Capsule<Real>) -> Result<CapsuleHandle, CreationErr> {
        let handle = Self::create(&mut self.capsules, capsule)?;
        Ok(CapsuleHandle(handle))
    }

    pub unsafe fn remove_capsule(&mut self, handle: CapsuleHandle) -> Result<Capsule<Real>, DerefErr> {
        Self::remove(&mut self.capsules, handle.0)
    }

    pub unsafe fn update_capsule(&mut self, handle: CapsuleHandle, new_capsule: Capsule<Real>) -> Result<Capsule<Real>, DerefErr> {
        Self::update(&mut self.capsules, handle.0, new_capsule)
    }

    pub unsafe fn oriented_box_iter(&self) -> RbIter<Real, OrientedBox<Real>> {
        RbIter::new(self.boxes.data_start, self.boxes.length)
    }

    pub unsafe fn get_oriented_box(&mut self, handle: OrientedBoxHandle) -> Result<*mut OrientedBox<Real>, DerefErr> {
        Self::get(&mut self.boxes, handle.0)
    }

    pub unsafe fn create_oriented_box(&mut self, oriented_box: OrientedBox<Real>) -> Result<OrientedBoxHandle, CreationErr> {
        let handle = Self::create(&mut self.boxes, oriented_box)?;
        Ok(OrientedBoxHandle(handle))
    }

    pub unsafe fn remove_oriented_box(&mut self, handle: OrientedBoxHandle) -> Result<OrientedBox<Real>, DerefErr> {
        Self::remove(&mut self.boxes, handle.0)
    }

    pub unsafe fn update_oriented_box(&mut self, handle: OrientedBoxHandle, new_oriented_box: OrientedBox<Real>) -> Result<OrientedBox<Real>, DerefErr> {
        Self::update(&mut self.boxes, handle.0, new_oriented_box)
    }

    pub fn num_circles(&self) -> usize {
        self.circles.length
    }

    pub fn num_capsules(&self) -> usize {
        self.capsules.length
    }

    pub fn num_oriented_boxes(&self) -> usize {
        self.boxes.length
    }
}

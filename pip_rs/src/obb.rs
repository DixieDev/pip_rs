use crate::{Circle, Capsule, Rigidbody, RigidbodyData, RigidbodyType};
use crate::maths::{self, Bounds, Manifold, Vector2, point_to_plane_dist};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum SatCollision {
    Obj1,
    Obj2,
}

pub struct OrientedBoxConfig<Real: maths::Real> {
    pub position: Vector2<Real>,
    pub rotation: Real,
    pub velocity: Vector2<Real>,
    pub angular_velocity: Real,
    pub mass: Real,
    pub restitution_coefficient: Real,
    pub is_kinematic: bool,
    pub half_extents: Vector2<Real>,
}

impl<Real: maths::Real> Default for OrientedBoxConfig<Real> {
    fn default() -> Self {
        Self {
            position: Vector2::new(Real::ZERO, Real::ZERO),
            rotation: Real::ZERO,
            velocity: Vector2::new(Real::ZERO, Real::ZERO),
            angular_velocity: Real::ZERO,
            mass: Real::ONE,
            restitution_coefficient: Real::ONE,
            is_kinematic: false,
            half_extents: Vector2::new(Real::ONE, Real::ONE),
        }
    }
}

#[derive(Clone)]
pub struct OrientedBox<Real: maths::Real> {
    pub rb: RigidbodyData<Real>,
    pub half_extents: Vector2<Real>,
}

impl<Real: maths::Real> OrientedBox<Real> {
    pub fn new(config: OrientedBoxConfig<Real>) -> Self {
        Self {
            rb: RigidbodyData {
                position: config.position,
                prev_position: config.position,
                rotation: config.rotation,
                velocity: config.velocity,
                angular_velocity: config.angular_velocity,
                acceleration: Vector2::new(Real::ZERO, Real::ZERO),
                mass: config.mass,
                restitution_coefficient: config.restitution_coefficient,
                time_in_sleep: Real::ZERO,
                is_kinematic: config.is_kinematic,
                is_sleeping: false,
                inertia: {
                    let width = config.half_extents.x.doubled();
                    let height = config.half_extents.y.doubled();
                    config.mass * (width*width + height*height).idiv(12)
                },
            },
            half_extents: config.half_extents,
        }
    }

    fn test_axis(
        axis: Vector2<Real>, 
        pos1: Vector2<Real>,
        pos2: Vector2<Real>,
        rot_extents1: Vector2<Real>,
        rot_extents2: Vector2<Real>,
        penetration: &mut Real,
    ) -> bool 
    {
        let pos1_axis = pos1.dot(axis);
        let pos2_axis = pos2.dot(axis);

        let p1 = [
            rot_extents1,
            -rot_extents1,
            rot_extents1.perpindicular(),
            -rot_extents1.perpindicular(),
        ];

        let p2 = [
            rot_extents2,
            -rot_extents2,
            rot_extents2.perpindicular(),
            -rot_extents2.perpindicular(),
        ];
        let (mut min1, mut max1) = (Real::ZERO, Real::ZERO);
        let (mut min2, mut max2) = (Real::ZERO, Real::ZERO);

        for i in 0..4 {
            let projection1 = p1[i].dot(axis);
            let projection2 = p2[i].dot(axis);

            if projection1 < min1 { min1 = projection1; }
            if projection1 > max1 { max1 = projection1; }
            if projection2 < min2 { min2 = projection2; }
            if projection2 > max2 { max2 = projection2; }
        }

        if pos1_axis < pos2_axis {
            if pos1_axis + max2 < pos2_axis + min2 {
                false
            } else {
                *penetration = (pos1_axis + max1) - (pos2_axis + min2);
                true
            }
        } else {
            if pos2_axis + max2 < pos1_axis + min1 {
                false
            } else {
                *penetration = (pos2_axis + max2) - (pos1_axis + min1);
                true
            }
        }
    }

    fn resolve_norms_dists_points(
        position1: Vector2<Real>,
        rotation1: Real,
        half_extents1: Vector2<Real>,
        position2: Vector2<Real>,
        rot_extents2: Vector2<Real>,
    ) -> ([Vector2<Real>; 4], [Real; 4], [Vector2<Real>; 4])
    {
        let plane_normals = [
            Vector2::new(Real::ONE, Real::ZERO).rotated(rotation1),
            -(Vector2::new(Real::ONE, Real::ZERO).rotated(rotation1)),
            Vector2::new(Real::ZERO, Real::ONE).rotated(rotation1),
            -(Vector2::new(Real::ZERO, Real::ONE).rotated(rotation1)),
        ];

        let plane_dists = [
            (position1 + plane_normals[0] * half_extents1.x).dot(plane_normals[0]),
            (position1 + plane_normals[1] * half_extents1.x).dot(plane_normals[1]),
            (position1 + plane_normals[2] * half_extents1.x).dot(plane_normals[2]),
            (position1 + plane_normals[3] * half_extents1.x).dot(plane_normals[3]),
        ];

        let box_points = [
            position2 + rot_extents2,
            position2 + rot_extents2.perpindicular(),
            position2 - rot_extents2,
            position2 - rot_extents2.perpindicular(),
        ];

        (plane_normals, plane_dists, box_points)
    }
}

impl<Real: maths::Real> Rigidbody<Real> for OrientedBox<Real> {
    const TYPE: RigidbodyType = RigidbodyType::OBB;

    fn rb(&self) -> &RigidbodyData<Real> { &self.rb }
    fn rb_mut(&mut self) -> &mut RigidbodyData<Real> { &mut self.rb }

    fn intersect_bounds(&self, bounds: Bounds<Real>) -> bool {
        let Bounds { bottom_left,  top_right } = bounds;
        let rot_extents = self.half_extents.rotated(self.rb.rotation);
        let quad_center = top_right + (bottom_left - top_right).halved();

        let mut dummy_penetration = Real::ZERO;

        let axes = [
            Vector2::new(Real::ONE, Real::ZERO),
            Vector2::new(Real::ZERO, Real::ONE),
            Vector2::new(Real::ONE, Real::ZERO).rotated(self.rb.rotation),
            Vector2::new(Real::ZERO, Real::ONE).rotated(self.rb.rotation),
        ];

        for axis in axes.iter().cloned() {
            let test = Self::test_axis (
                axis,
                self.rb.position,
                quad_center, 
                rot_extents,
                top_right,
                &mut dummy_penetration,
            );

            if !test { return false; }
        }

        true
    }

    fn intersect_circle(&self, other: &Circle<Real>, manifold: &mut Manifold<Real>) -> bool {
        other.intersect_obb(self, manifold)
    }

    fn intersect_capsule(&self, other: &Capsule<Real>, manifold: &mut Manifold<Real>) -> bool {
        other.intersect_obb(self, manifold)
    }

    fn intersect_obb(&self, other: &OrientedBox<Real>, manifold: &mut Manifold<Real>) -> bool {
        let position1 = self.rb.position;
        let position2 = other.rb.position;
        let rotation1 = self.rb.rotation;
        let rotation2 = other.rb.rotation;

        let a_to_b = position2 - position1;
        let rot_extents1 = self.half_extents.rotated(rotation1);
        let rot_extents2 = other.half_extents.rotated(rotation2);

        let mut min_penetration = Real::ZERO;
        let mut min_axis = Vector2::new(Real::ZERO, Real::ZERO);
        let mut penetration = Real::ZERO;
        let mut collision_type = SatCollision::Obj1;

        let axes = [
            (Vector2::new(Real::ONE, Real::ZERO).rotated(rotation1), SatCollision::Obj1),
            (Vector2::new(Real::ZERO, Real::ONE).rotated(rotation1), SatCollision::Obj1),
            (Vector2::new(Real::ONE, Real::ZERO).rotated(rotation2), SatCollision::Obj2),
            (Vector2::new(Real::ZERO, Real::ONE).rotated(rotation2), SatCollision::Obj2),
        ];

        for (axis, sat_type) in axes.iter().cloned() {
            let test = Self::test_axis( 
                axis,
                position1,
                position2,
                rot_extents1,
                rot_extents2,
                &mut penetration,
            );

            if test {
                if penetration < min_penetration {
                    min_penetration = penetration;
                    min_axis = axis;
                    collision_type = sat_type;
                }
            } else {
                return false;
            }
        }

        let (plane_normals, plane_dists, box_points) = match collision_type {
            SatCollision::Obj1 => Self::resolve_norms_dists_points (
                position1,
                rotation1,
                self.half_extents,
                position2,
                rot_extents2,
            ),
            SatCollision::Obj2 => Self::resolve_norms_dists_points (
                position2,
                rotation2,
                other.half_extents,
                position1,
                rot_extents1,
            ),
        };

        for i in 0..4 {
            let next_idx = if i+1 < 4 { i + 1 } else { 0 };
            let next_pt = box_points[next_idx];
            let mut next_pt_in = true;

            for j in 0..4 {
                if point_to_plane_dist(next_pt, plane_normals[j], plane_dists[j]) > Real::ZERO {
                    next_pt_in = false
                }
            }

            if next_pt_in {
                manifold.contact_points[manifold.num_contact_points] = next_pt;
                manifold.num_contact_points += 1;
            }
        }

        manifold.normal = if min_axis.dot(a_to_b) > Real::ZERO { -min_axis } else { min_axis };
        manifold.penetration = min_penetration;

        true
    }

    fn sweep_circle(&self, _other: &Circle<Real>, _dt: Real, _manifold: &mut Manifold<Real>) -> Real {
        Real::ZERO
    }

    fn sweep_capsule(&self, _other: &Capsule<Real>, _dt: Real, _manifold: &mut Manifold<Real>) -> Real {
        Real::ZERO
    }

    fn sweep_obb(&self, _other: &OrientedBox<Real>, _dt: Real, _manifold: &mut Manifold<Real>) -> Real {
        Real::ZERO
    }
}

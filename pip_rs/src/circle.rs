use crate::{ Rigidbody, RigidbodyData, RigidbodyType, Capsule, OrientedBox };
use crate::maths::{self, Bounds, Manifold, Vector2, closest_point_to_segment};

pub struct CircleConfig<Real: maths::Real> {
    pub radius: Real,
    pub position: Vector2<Real>,
    pub rotation: Real,
    pub velocity: Vector2<Real>,
    pub angular_velocity: Real,
    pub mass: Real,
    pub restitution_coefficient: Real,
    pub is_kinematic: bool,
}

impl<Real: maths::Real> Default for CircleConfig<Real> {
    fn default() -> Self {
        Self {
            radius: Real::ONE,
            position: Vector2::new(Real::ZERO, Real::ZERO),
            rotation: Real::ZERO,
            velocity: Vector2::new(Real::ZERO, Real::ZERO),
            angular_velocity: Real::ZERO,
            mass: Real::ONE,
            restitution_coefficient: Real::ONE,
            is_kinematic: false,
        }
    }
}

#[derive(Clone)]
pub struct Circle<Real: maths::Real> {
    pub rb: RigidbodyData<Real>,
    pub radius: Real,
}

impl<Real: maths::Real> Circle<Real> {
    pub fn new(config: CircleConfig<Real>) -> Self {
        Self {
            rb: RigidbodyData {
                position: config.position,
                prev_position: config.position,
                rotation: config.rotation,
                velocity: config.velocity,
                angular_velocity: config.angular_velocity,
                acceleration: Vector2::new(Real::ZERO, Real::ZERO),
                mass: config.mass,
                restitution_coefficient: config.restitution_coefficient,
                time_in_sleep: Real::ZERO,
                is_kinematic: config.is_kinematic,
                is_sleeping: false,
                inertia: config.mass * config.radius * config.radius,
            },
            radius: config.radius,
        }
    }
}

#[allow(unused_variables)]
impl<Real: maths::Real> Rigidbody<Real> for Circle<Real> {
    const TYPE: RigidbodyType = RigidbodyType::Circle;

    fn rb(&self) -> &RigidbodyData<Real> { &self.rb }
    fn rb_mut(&mut self) -> &mut RigidbodyData<Real> { &mut self.rb }

    fn intersect_bounds(&self, bounds: Bounds<Real>) -> bool {
        let Bounds { top_right, bottom_left } = bounds;
        let pos = self.rb.position;
        let clamped_pos = Vector2 {
            x: pos.x.pclamp(bottom_left.x, top_right.x),
            y: pos.y.pclamp(bottom_left.y, top_right.y),
        };

        (self.radius * self.radius) >= (pos - clamped_pos).length_squared()
    }

    fn intersect_circle(&self, other: &Circle<Real>, manifold: &mut Manifold<Real>) -> bool {
        let ab = other.rb.position - self.rb.position;
        let max_dist = self.radius + other.radius;
        if ab.length_squared() <= max_dist*max_dist {
            manifold.penetration = max_dist - ab.length();

            let normalized = ab.normalized();
            manifold.normal = -normalized;
            
            let circle1_edge = self.rb.position + normalized * self.radius;
            let circle2_edge = other.rb.position - normalized * other.radius;
            manifold.num_contact_points = 1;
            manifold.contact_points[0] = circle1_edge + (circle2_edge - circle1_edge).halved();

            true
        } else {
            false
        }
    }

    fn intersect_capsule(&self, other: &Capsule<Real>, manifold: &mut Manifold<Real>) -> bool {
        let half_length = other.length.halved();
        let a = {
            let mut a = Vector2::new(-half_length, Real::ZERO);
            a.rotate(other.rb.rotation);
            a + other.rb.position
        };

        let b = {
            let mut b = Vector2::new(half_length, Real::ZERO);
            b.rotate(other.rb.rotation);
            b + other.rb.position
        };

        let c = self.rb.position;
        let rab = self.radius + other.radius;
        
        let closest_point = closest_point_to_segment(a, b, c);
        let closest_vec = closest_point - c;
        if closest_vec.length_squared() <= rab*rab {
            manifold.penetration = rab - closest_vec.length();
            let normalized = closest_vec.normalized();
            let capsule_edge = closest_point - normalized * other.radius;
            let sphere_edge = c + normalized * self.radius;
            manifold.normal = -normalized;
            manifold.num_contact_points = 1;
            manifold.contact_points[0] = (capsule_edge + sphere_edge).halved();

            true
        } else {
            false
        }
    }

    fn intersect_obb(&self, other: &OrientedBox<Real>, manifold: &mut Manifold<Real>) -> bool {
        false
    }

    fn sweep_circle(&self, other: &Circle<Real>, dt: Real, manifold: &mut Manifold<Real>) -> Real {
        let disp1 = self.rb.velocity * dt;
        let disp2 = other.rb.velocity * dt;

        let ab = other.rb.position - self.rb.position;
        let disp_ab = disp2 - disp1;

        let max_dist = self.radius + other.radius;

        let a = disp_ab.length_squared();
        let b = disp_ab.dot(ab).doubled();
        let c = ab.length_squared() - max_dist*max_dist;

        let q = b*b - (a*c).quadrupled();
        if q < Real::ZERO {
            Real::ZERO
        } else {
            let sqrt = q.sqrt();
            let d = a.doubled();

            let root1 = (-b + sqrt) / d;
            let root2 = (-b - sqrt) / d;
            let real_root = if root1 <= root2 { root1 } else { root2 };

            let rb1_pos = self.rb.position + disp1*real_root;
            let rb2_pos = other.rb.position + disp2*real_root;
            manifold.contact_points[0] = rb1_pos + (rb2_pos - rb1_pos) * (self.radius / max_dist);
            manifold.normal = (rb1_pos - rb2_pos).normalized();

            real_root
        }
    }

    fn sweep_capsule(&self, other: &Capsule<Real>, dt: Real, manifold: &mut Manifold<Real>) -> Real {
        self.radius
    }

    fn sweep_obb(&self, other: &OrientedBox<Real>, dt: Real, manifold: &mut Manifold<Real>) -> Real {
        self.radius
    }
}

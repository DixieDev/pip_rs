use crate::{Rigidbody, RigidbodyData, RigidbodyType, Circle, OrientedBox};
use crate::maths::{self, Bounds, Manifold, Vector2};

pub struct CapsuleConfig<Real: maths::Real> {
    position: Vector2<Real>,
    rotation: Real,
    velocity: Vector2<Real>,
    angular_velocity: Real,
    mass: Real,
    restitution_coefficient: Real,
    is_kinematic: bool,
    length: Real,
    radius: Real,
}

#[derive(Clone)]
pub struct Capsule<Real: maths::Real> {
    pub rb: RigidbodyData<Real>,
    pub length: Real,
    pub radius: Real,
}

impl<Real: maths::Real> Capsule<Real> {
    pub fn new(config: CapsuleConfig<Real>) -> Self {
        Self {
            rb: RigidbodyData {
                position: config.position,
                prev_position: config.position,
                rotation: config.rotation,
                velocity: config.velocity,
                angular_velocity: config.angular_velocity,
                acceleration: Vector2::new(Real::ZERO, Real::ZERO),
                mass: config.mass,
                restitution_coefficient: config.restitution_coefficient,
                time_in_sleep: Real::ZERO,
                is_kinematic: config.is_kinematic,
                is_sleeping: false,
                inertia: {
                    let rad_sqr = config.radius * config.radius;
                    let diameter = config.radius.doubled();

                    let area_hemicircles = rad_sqr * Real::PI;
                    let area_rectangle = config.length * diameter;
                    let area = area_hemicircles + area_rectangle;

                    let mass_hemicircles = config.mass * area_hemicircles / area;
                    let mass_rectangle = config.mass - mass_hemicircles;
                    let diameter_sqr = diameter*diameter;
                    let length_sqr = config.length*config.length;
                    let inertia_rectangle = mass_rectangle * (diameter_sqr + length_sqr).idiv(12);

                    let inertia_circle1 = mass_hemicircles * rad_sqr.halved();
                    let inertia_circle2 = inertia_circle1 - mass_hemicircles * config.length.halved();

                    inertia_rectangle + inertia_circle2
                },
            },
            length: config.length,
            radius: config.radius,
        }
    }
}

impl<Real: maths::Real> Rigidbody<Real> for Capsule<Real> {
    const TYPE: RigidbodyType = RigidbodyType::Capsule;

    fn rb(&self) -> &RigidbodyData<Real> { &self.rb }
    fn rb_mut(&mut self) -> &mut RigidbodyData<Real> { &mut self.rb }

    fn intersect_bounds(&self, bounds: Bounds<Real>) -> bool {
        let top_right = bounds.top_right;
        let bottom_left = bounds.bottom_left;
        let rad_sqr = self.radius * self.radius;

        let m = self.rb.rotation.tan();

        let half_length = self.length.halved();

        let mut a = Vector2::new(-half_length, Real::ZERO);
        let mut b = Vector2::new(half_length, Real::ZERO);
        a.rotate(self.rb.rotation);
        b.rotate(self.rb.rotation);
        a = a + self.rb.position;
        b = b + self.rb.position;

        let c = a.y - a.x*m;

        let a_clamped = Vector2 {
            x: a.x.pclamp(bottom_left.x, top_right.x),
            y: a.y.pclamp(bottom_left.y, top_right.y),
        };

        let b_clamped = Vector2 {
            x: b.x.pclamp(bottom_left.x, top_right.y),
            y: a.y.pclamp(bottom_left.y, top_right.y),
        };

        if (a_clamped-a).length_squared() <= rad_sqr 
            || (b_clamped-a).length_squared() <= rad_sqr 
        {
            return true;
        } 

        let (segment_x_min, segment_x_max) = if a.x <= b.x { (a.x, b.x) } else { (b.x, a.x) };
        let (segment_y_min, segment_y_max) = if a.y <= b.y { (a.y, b.y) } else { (b.y, a.y) };

        if m == Real::ZERO {
            let y = a.y;
            let x = bottom_left.x;
            let x2 = top_right.y;
            let box_clamp_y = y.pclamp(bottom_left.y, top_right.y);
            let box_clamp1 = Vector2::new(bottom_left.x, box_clamp_y);
            let box_clamp2 = Vector2::new(top_right.x, box_clamp_y);

            let segment_clamp1 = Vector2::new(x.pclamp(segment_x_min, segment_x_max), y);
            let segment_clamp2 = Vector2::new(x2.pclamp(segment_x_min, segment_x_max), y);

            if (segment_clamp1 - box_clamp1).length_squared() <= rad_sqr { return true; }
            if (segment_clamp1 - box_clamp2).length_squared() <= rad_sqr { return true; }
            if (segment_clamp2 - box_clamp1).length_squared() <= rad_sqr { return true; }
            if (segment_clamp2 - box_clamp2).length_squared() <= rad_sqr { return true; }
        } else if m == Real::MAX {
            let x = a.x;
            let y = bottom_left.y;
            let y2 = top_right.y;
            let box_clamp_x = x.pclamp(bottom_left.x, top_right.x);
            let box_clamp1 = Vector2::new(box_clamp_x, bottom_left.y);
            let box_clamp2 = Vector2::new(box_clamp_x, top_right.y);

            let segment_clamp1 = Vector2::new(x, y.pclamp(segment_y_min, segment_y_max));
            let segment_clamp2 = Vector2::new(x, y2.pclamp(segment_y_min, segment_y_max));

            if (segment_clamp1 - box_clamp1).length_squared() <= rad_sqr { return true; }
            if (segment_clamp1 - box_clamp2).length_squared() <= rad_sqr { return true; }
            if (segment_clamp2 - box_clamp1).length_squared() <= rad_sqr { return true; }
            if (segment_clamp2 - box_clamp2).length_squared() <= rad_sqr { return true; }
        } else {
            let line_x1 = Vector2::new(bottom_left.x, m * bottom_left.x + c);
            let line_x2 = Vector2::new(top_right.x, m * top_right.x + c);
            let line_y1 = Vector2::new((bottom_left.y - c) / m, bottom_left.y);
            let line_y2 = Vector2::new((top_right.y - c) / m, top_right.y);

            let check = move |box_clamp: Vector2<Real>, line: Vector2<Real>| {
                let segment_clamp = Vector2 {
                    x: line.x.pclamp(segment_x_min, segment_x_max),
                    y: line.y.pclamp(segment_y_min, segment_y_max),
                };

                (box_clamp - segment_clamp).length_squared() <= rad_sqr
            };

            let box_clamp1 = Vector2 {
                x: bottom_left.x, 
                y: line_x1.y.pclamp(bottom_left.y, top_right.y),
            };
            if (check)(box_clamp1, line_x1) { return true; }

            let box_clamp2 = Vector2 {
                x: top_right.x,
                y: line_x2.y.pclamp(bottom_left.x, top_right.y),
            };
            if (check)(box_clamp2, line_x2) { return true; }

            let box_clamp3 = Vector2 {
                x: line_y1.x.pclamp(bottom_left.x, top_right.x),
                y: bottom_left.y,
            };
            if (check)(box_clamp3, line_y1) { return true; }

            let box_clamp4 = Vector2 {
                x: line_y2.x.pclamp(bottom_left.x, top_right.x),
                y: top_right.y,
            };
            if (check)(box_clamp4, line_y2) { return true; }
        }

        false
    }

    fn intersect_circle(&self, _other: &Circle<Real>, _manifold: &mut Manifold<Real>) -> bool {
        false
    }

    fn intersect_capsule(&self, _other: &Capsule<Real>, _manifold: &mut Manifold<Real>) -> bool {
        false
    }

    fn intersect_obb(&self, _other: &OrientedBox<Real>, _manifold: &mut Manifold<Real>) -> bool {
        false
    }

    fn sweep_circle(&self, _other: &Circle<Real>, _dt: Real, _manifold: &mut Manifold<Real>) -> Real {
        self.length
    }

    fn sweep_capsule(&self, _other: &Capsule<Real>, _dt: Real, _manifold: &mut Manifold<Real>) -> Real {
        self.length
    }

    fn sweep_obb(&self, _other: &OrientedBox<Real>, _dt: Real, _manifold: &mut Manifold<Real>) -> Real {
        self.length
    }
}

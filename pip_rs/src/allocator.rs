#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum AllocErr {
    OutOfMemory,
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum FreeErr {
    InvalidAddress,
    NotAllocated,
}

pub trait Allocator {
    unsafe fn alloc(&mut self, bytes: usize) -> Result<*mut u8, AllocErr>;
    unsafe fn free(&mut self, ptr: *mut u8) -> Result<(), FreeErr>;
}

pub struct DefaultAllocator {
    allocated: bool,
    bytes: Vec<u8>,
}

impl DefaultAllocator {
    pub fn new(capacity: usize) -> Self {
        Self {
            allocated: false,
            bytes: vec![0u8; capacity],
        }
    }

    pub fn allocated(&self) -> bool {
        self.allocated
    }
}

impl Allocator for DefaultAllocator {
    unsafe fn alloc(&mut self, bytes: usize) -> Result<*mut u8, AllocErr> {
        if !self.allocated && bytes <= self.bytes.len() {
            self.allocated = true;
            Ok(self.bytes.as_mut_ptr())
        } else {
            Err(AllocErr::OutOfMemory)
        }
    }

    unsafe fn free(&mut self, ptr: *mut u8) -> Result<(), FreeErr> {
        if self.allocated {
            if self.bytes.as_mut_ptr() == ptr {
                self.allocated = false;
                Ok(())
            } else {
                Err(FreeErr::InvalidAddress)
            }
        } else {
            Err(FreeErr::NotAllocated)
        }
    }
}

#[cfg(test)]
mod test {
    use super::{Allocator, AllocErr, DefaultAllocator};

    #[test]
    fn alloc_alloc_free_alloc() {
        // Allocates twice, frees, then attempts a final allocation

        let mut allocator = DefaultAllocator::new(128);

        let result = unsafe { allocator.alloc(128) };
        let ptr = match result {
            Ok(ptr) => ptr,
            Err(e) => panic!("Failed first allocation: {:?}", e),
        };

        let result = unsafe { allocator.alloc(128) };
        match result {
            Ok(_) => panic!("Second allocation succeeded, this is wrong"),
            Err(AllocErr::OutOfMemory) => (),
        }

        let result = unsafe { allocator.free(ptr) };
        match result {
            Ok(_) => (),
            Err(e) => panic!("Failed to free: {:?}", e),
        }

        let result = unsafe { allocator.alloc(128) };
        match result {
            Ok(_) => (),
            Err(e) => panic!("Failed to alloc after free: {:?}", e)
        }
    }

    #[test]
    fn alloc_too_much() {
        // Allocates more bytes than available in the DefaultAllocator's buffer

        let mut allocator = DefaultAllocator::new(128);
        let result = unsafe { allocator.alloc(256) };
        match result {
            Ok(_) => panic!("Allocation reported success for more than the available bytes"),
            Err(_) => (),
        }

        let result = unsafe { allocator.alloc(128) };
        match result {
            Ok(_) => (),
            Err(e) => panic!("Correct allocation failed after failing previous allocation: {:?}", e),
        }
    }

    #[test]
    fn free_unallocated_ptr() {
        // Frees before calling alloc

        let mut allocator = DefaultAllocator::new(128);

        let result = unsafe {
            let ptr = allocator.bytes.as_mut_ptr();
            allocator.free(ptr)
        };

        match result {
            Ok(_) => panic!("Successfully freed despite no initial allocation"),
            Err(_) => (),
        }
    }
}

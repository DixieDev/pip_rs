use crate::{Circle, Capsule, OrientedBox, Rigidbody, RigidbodyData};
use crate::maths::{self, Manifold, Vector2};
use crate::memory::{CapsuleHandle, CircleHandle, CreationErr, DerefErr, Memory, MemoryConfig, OrientedBoxHandle};
use crate::allocator::{Allocator, AllocErr};

pub struct WorldConfig<Real: maths::Real, A: Allocator> {
    pub allocator: A,
    pub memory_config: MemoryConfig,
    pub step_mode: bool,
    pub step_once: bool,
    pub timestep: Real,
    pub max_accumulation: Real,
    pub gravity: Real,
    pub static_resolution: bool,
    pub ignore_separating_bodies: bool,
}

pub struct World<Real: maths::Real, A: Allocator> {
    allocator: A,
    memory: Memory<Real>,
    step_mode: bool,
    step_once: bool,
    timestep: Real,
    accumulator: Real,
    max_accumulation: Real,
    gravity: Real,
    static_resolution: bool,
    ignore_separating_bodies: bool,
}

impl<Real: maths::Real, A: Allocator> World<Real, A> {
    pub fn new(config: WorldConfig<Real, A>) -> Result<Self, AllocErr> {
        let WorldConfig { 
            mut allocator,
            memory_config ,
            step_mode,
            step_once,
            timestep,
            max_accumulation,
            gravity,
            static_resolution,
            ignore_separating_bodies,
        } = config;

        let memory = unsafe { Memory::new(&mut allocator, memory_config)? };
        Ok(Self {
            allocator,
            memory,
            step_mode,
            step_once,
            timestep,
            max_accumulation,
            accumulator: Real::ZERO,
            gravity,
            static_resolution,
            ignore_separating_bodies,
        })
    }

    pub fn update(&mut self, dt: Real) {
        if self.step_mode {
            if self.step_once {
                self.step(self.timestep);
            } 
        } else {
            self.accumulator = self.accumulator + dt;
            if self.max_accumulation > Real::ZERO && self.accumulator > self.max_accumulation {
                self.accumulator = self.max_accumulation;
            }

            while self.accumulator >= self.timestep {
                self.step(self.timestep);
                self.accumulator = self.accumulator - self.timestep;
            }
        }
    }

    fn step(&mut self, dt: Real) {
        self.integrate_rigidbodies(dt);
        self.handle_intersections(dt);
    }

    fn handle_intersections(&mut self, dt: Real) {
        unsafe { 
            let mut circles_a = self.memory.circle_iter();

            for _ in 0..self.memory.num_circles()-1 {
                let a_ptr = match circles_a.next() {
                    Some(ptr) => ptr,
                    None => break,
                };

                for b_ptr in circles_a.clone() {
                    let skip_a = (*a_ptr).rb.is_sleeping || (*a_ptr).rb.is_kinematic;
                    let skip_b = (*b_ptr).rb.is_sleeping || (*b_ptr).rb.is_kinematic;

                    if skip_a && skip_b {
                        continue;
                    } 

                    let mut manifold = Manifold::default();
                    if (*a_ptr).intersect_circle(&mut (*b_ptr), &mut manifold) {
                        self.compute_response(
                            [&mut (*a_ptr).rb, &mut (*b_ptr).rb], 
                            manifold
                        );
                    }
                }

                self.handle_sleep(&mut (*a_ptr).rb, dt);
            }
        }
    }

    fn handle_sleep(&self, rb: &mut RigidbodyData<Real>, dt: Real) {
        if !rb.is_kinematic {
            let displacement = rb.position - rb.prev_position;
            let eps_sqr = Real::EPSILON * Real::EPSILON;
            if displacement.length_squared() <= eps_sqr {
                rb.time_in_sleep = rb.time_in_sleep + dt;

                if !rb.is_sleeping && rb.time_in_sleep > self.timestep.doubled() {
                    rb.is_sleeping = true;
                    rb.velocity = Vector2::new(Real::ZERO, Real::ZERO);
                }
            } else {
                if rb.is_sleeping {
                    rb.is_sleeping = false;
                }
                rb.time_in_sleep = Real::ZERO;
            }
        }
    }

    fn compute_response(&mut self, bodies: [&mut RigidbodyData<Real>; 2], manifold: Manifold<Real>) {
        let [rb1, rb2] = bodies;

        let n = manifold.normal;
        let pen = manifold.penetration;
        let inv_mass_a = if rb1.is_kinematic { Real::ZERO } else { Real::ONE / rb1.mass };
        let inv_mass_b = if rb2.is_kinematic { Real::ZERO } else { Real::ONE / rb2.mass };
        let inv_i_a = if rb1.is_kinematic { Real::ZERO } else { Real::ONE / rb1.inertia };
        let inv_i_b = if rb2.is_kinematic { Real::ZERO } else { Real::ONE / rb2.inertia };
        let mut result_vel_a = rb1.velocity;
        let mut result_vel_b = rb2.velocity;
        let mut result_ang_vel_a = rb1.angular_velocity;
        let mut result_ang_vel_b = rb2.angular_velocity;
        let e = (rb1.restitution_coefficient * rb2.restitution_coefficient).sqrt();
        let avg_contact_point = {
            let mut point = Vector2::new(Real::ZERO, Real::ZERO);
            let num_points = if manifold.num_contact_points > i16::MAX as usize { 
                i16::MAX 
            } else { 
                manifold.num_contact_points as i16
            };

            for i in 0..num_points {
                let i = i as usize;
                point = point + manifold.contact_points[i];
            }

            Vector2 {
                x: point.x.idiv(num_points),
                y: point.y.idiv(num_points),
            }
        };

        let ra = avg_contact_point - rb1.position;
        let rb = avg_contact_point - rb2.position;
        let ra_p = ra.perpindicular();
        let rb_p = rb.perpindicular();
        let va = rb1.velocity + ra_p * rb1.angular_velocity;
        let vb = rb2.velocity + rb_p * rb2.angular_velocity;
        let vba = va - vb;
        let vba_dot_n = vba.dot(n);
        if self.static_resolution {
            let disp_factor = if rb1.is_kinematic { 
                Real::ZERO 
            } else if rb2.is_kinematic {
                Real::ONE
            } else {
                Real::ONE.halved()
            };

            rb1.position = rb1.position + n * pen * disp_factor;
            rb2.position = rb2.position - n * pen * disp_factor;
        } 

        if self.ignore_separating_bodies {
            if vba_dot_n >= Real::ZERO {
                return;
            }
        }

        assert!(vba_dot_n < Real::ZERO);
        
        let num = -(Real::ONE + e) * vba_dot_n;
        let ra_p_dot_sqr = ra_p.dot(n) * ra_p.dot(n);
        let rb_p_dot_sqr = rb_p.dot(n) * rb_p.dot(n);
        let denom = inv_mass_a + inv_mass_b + ra_p_dot_sqr * inv_i_a + rb_p_dot_sqr * inv_i_b;
        let impulse_reactionary = num / denom;
        assert!(impulse_reactionary > Real::ZERO);
        result_vel_a = result_vel_a + n * impulse_reactionary * inv_mass_a;
        result_ang_vel_a = result_ang_vel_a + ra_p.dot(n * impulse_reactionary) * inv_i_a;
        result_vel_b = result_vel_b - n * impulse_reactionary * inv_mass_b;
        result_ang_vel_b = result_ang_vel_b - rb_p.dot(n * impulse_reactionary) * inv_i_b;

        // TODO: Friction

        rb1.velocity = result_vel_a;
        rb1.angular_velocity = result_ang_vel_a;
        rb2.velocity = result_vel_b;
        rb2.angular_velocity = result_ang_vel_b;
    }

    fn integrate_rigidbodies(&mut self, dt: Real) {
        unsafe {
            for circle_ptr in self.memory.circle_iter() {
                self.integrate_rb(&mut (*circle_ptr).rb, dt);
            }

            for capsule_ptr in self.memory.capsule_iter() {
                self.integrate_rb(&mut (*capsule_ptr).rb, dt);
            }

            for oriented_box_ptr in self.memory.oriented_box_iter() {
                self.integrate_rb(&mut (*oriented_box_ptr).rb, dt);
            }
        }
    }

    unsafe fn integrate_rb(&mut self, rb: &mut RigidbodyData<Real>, dt: Real) {
        let y_accel = -self.gravity / rb.mass;
        rb.acceleration = rb.acceleration + Vector2::new(Real::ZERO, y_accel);

        if !(rb.is_kinematic || rb.is_sleeping) {
            rb.velocity = rb.velocity + rb.acceleration * dt;
        }

        rb.prev_position = rb.position;
        rb.position = rb.position + rb.velocity * dt;
        rb.rotation = rb.rotation + rb.angular_velocity * dt;
        rb.acceleration = Vector2::new(Real::ZERO, Real::ZERO);
    }

    pub fn create_circle(&mut self, circle: Circle<Real>) -> Result<CircleHandle, CreationErr> {
        unsafe { self.memory.create_circle(circle) }
    }

    pub fn create_capsule(&mut self, capsule: Capsule<Real>) -> Result<CapsuleHandle, CreationErr> {
        unsafe { self.memory.create_capsule(capsule) }
    }

    pub fn create_oriented_box(&mut self, oriented_box: OrientedBox<Real>) -> Result<OrientedBoxHandle, CreationErr> {
        unsafe { self.memory.create_oriented_box(oriented_box) }
    }

    pub fn get_circle(&mut self, handle: CircleHandle) -> Result<Circle<Real>, DerefErr> {
        unsafe {
            let ptr = self.memory.get_circle(handle)?;
            Ok((*ptr).clone())
        }
    }

    pub fn get_capsule(&mut self, handle: CapsuleHandle) -> Result<Capsule<Real>, DerefErr> {
        unsafe {
            let ptr = self.memory.get_capsule(handle)?;
            Ok((*ptr).clone())
        }
    }

    pub fn get_oriented_box(&mut self, handle: OrientedBoxHandle) -> Result<OrientedBox<Real>, DerefErr> {
        unsafe {
            let ptr = self.memory.get_oriented_box(handle)?;
            Ok((*ptr).clone())
        }
    }

    pub fn update_circle(&mut self, handle: CircleHandle, new_circle: Circle<Real>) -> Result<Circle<Real>, DerefErr> {
        unsafe { self.memory.update_circle(handle, new_circle) }
    }

    pub fn update_capsule(&mut self, handle: CapsuleHandle, new_capsule: Capsule<Real>) -> Result<Capsule<Real>, DerefErr> {
        unsafe { self.memory.update_capsule(handle, new_capsule) }
    }

    pub fn update_oriented_box(&mut self, handle: OrientedBoxHandle, new_oriented_box: OrientedBox<Real>) -> Result<OrientedBox<Real>, DerefErr> {
        unsafe { self.memory.update_oriented_box(handle, new_oriented_box) }
    }

    pub fn remove_circle(&mut self, handle: CircleHandle) -> Result<Circle<Real>, DerefErr> {
        unsafe { self.memory.remove_circle(handle) }
    }

    pub fn remove_capsule(&mut self, handle: CapsuleHandle) -> Result<Capsule<Real>, DerefErr> {
        unsafe { self.memory.remove_capsule(handle) }
    }

    pub fn remove_oriented_box(&mut self, handle: OrientedBoxHandle) -> Result<OrientedBox<Real>, DerefErr> {
        unsafe { self.memory.remove_oriented_box(handle) }
    }
}

impl<Real: maths::Real, A: Allocator> Drop for World<Real, A> {
    fn drop(&mut self) {
        unsafe { self.memory.free(&mut self.allocator).unwrap(); }
    }
}

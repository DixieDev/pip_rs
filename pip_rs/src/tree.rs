pub struct QuadNodeHandle {
    idx: usize
}

pub struct QuadNode<Real: math::Real> {
    owner: *mut QuadNode<
    children: MemoryView<QuadNode<Real>>,
    bounds: Bounds<Real>,
    is_leaf: bool,
}

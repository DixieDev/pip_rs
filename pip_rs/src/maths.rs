
use std::ops::{Add, Sub, Mul, Div, Neg};
use std::cmp::PartialOrd;
use std::cmp::Ordering::*;

pub trait Real : Sized + Clone + Copy + PartialOrd
    + Add<Output=Self>
    + Sub<Output=Self>
    + Mul<Output=Self>
    + Div<Output=Self>
    + Neg<Output=Self>
{
    const ZERO: Self;
    const ONE: Self;
    const MAX: Self;
    const EPSILON: Self;
    const PI: Self;

    /// Rust's most useful clamp func for generic programming is tied to the `Ord` trait, which
    /// floats don't implement. Most numeric types will implement PartialOrd, so we use that
    /// here to allow for a more permissive clamp for use in this lib.
    fn pclamp(self, min: Self, max: Self) -> Self {
        match (self.partial_cmp(&min), self.partial_cmp(&max)) {
            (Some(Less), _) => min,
            (_, Some(Greater)) => max,
            _ => self,
        }
    }

    /// Oftentimes we need to divide by some constant which can be perfectly represented with
    /// an int. i16 was chosen for the input because it can be losslessly converted to an f32.
    fn idiv(self, n: i16) -> Self;

    fn halved(self) -> Self;
    fn doubled(self) -> Self;
    fn quadrupled(self) -> Self;
    fn sqrt(self) -> Self;
    fn sin(self) -> Self;
    fn cos(self) -> Self;
    fn tan(self) -> Self;
}

impl Real for f32 {
    const ZERO: Self = 0.0;
    const ONE: Self = 1.0;
    const MAX: Self = f32::MAX;
    const EPSILON: Self = Self::EPSILON;
    const PI: Self = core::f32::consts::PI;

    fn pclamp(self, min: Self, max: Self) -> Self {
        self.clamp(min, max)
    }

    fn idiv(self, n: i16) -> Self {
        self / (n as Self)
    }

    fn halved(self) -> Self { self / 2.0 }
    fn doubled(self) -> Self { self * 2.0 }
    fn quadrupled(self) -> Self { self * 4.0 }
    fn sqrt(self) -> Self { self.sqrt() }
    fn sin(self) -> Self { self.sin() }
    fn cos(self) -> Self { self.cos() }
    fn tan(self) -> Self { self.tan() }
}

impl Real for f64 {
    const ZERO: Self = 0.0;
    const ONE: Self = 1.0;
    const MAX: Self = f64::MAX;
    const EPSILON: Self = Self::EPSILON;
    const PI: Self = core::f64::consts::PI;

    fn pclamp(self, min: Self, max: Self) -> Self {
        self.clamp(min, max)
    }

    fn idiv(self, n: i16) -> Self {
        self / (n as Self)
    }

    fn halved(self) -> Self { self / 2.0 }
    fn doubled(self) -> Self { self * 2.0 }
    fn quadrupled(self) -> Self { self * 4.0 }
    fn sqrt(self) -> Self { self.sqrt() }
    fn sin(self) -> Self { self.sin() }
    fn cos(self) -> Self { self.cos() }
    fn tan(self) -> Self { self.tan() }
}

#[derive(Clone, Copy)]
pub struct Vector2<R: Real> {
    pub x: R,
    pub y: R,
}

impl<R: Real> Vector2<R> {
    pub fn new(x: R, y: R) -> Self {
        Self { x, y }
    }

    pub fn length_squared(self) -> R {
        (self.x * self.x) + (self.y * self.y)
    }

    pub fn length(self) -> R {
        ((self.x * self.x) + (self.y * self.y)).sqrt()
    }

    pub fn dot(self, other: Self) -> R {
        self.x * other.x + self.y * other.y
    }

    pub fn normalized(self) -> Self {
        let length = self.length();
        Self {
            x: self.x / length,
            y: self.y / length,
        }
    }

    pub fn halved(self) -> Self {
        Self {
            x: self.x.halved(),
            y: self.y.halved(),
        }
    }

    pub fn doubled(self) -> Self {
        Self {
            x: self.x.doubled(),
            y: self.y.doubled(),
        }
    }

    pub fn rotate(&mut self, rotation: R) {
        let cos = rotation.cos();
        let sin = rotation.sin();
        self.x = self.x*cos - self.y*sin;
        self.y = self.y*cos - self.x*sin;
    }

    pub fn rotated(self, rotation: R) -> Self {
        let cos = rotation.cos();
        let sin = rotation.sin();
        Self {
            x: self.x*cos - self.y*sin,
            y: self.y*cos - self.x*sin,
        }
    }

    pub fn perpindicular(self) -> Self {
        Self {
            x: self.y,
            y: -self.x,
        }
    }
}

impl<R: Real> Neg for Vector2<R> {
    type Output=Self;
    fn neg(self) -> Self {
        Self {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl<R: Real> Add for Vector2<R> {
    type Output=Self;
    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<R: Real> Sub for Vector2<R> {
    type Output=Self;
    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<R: Real> Mul for Vector2<R> {
    type Output=Self;
    fn mul(self, other: Self) -> Self {
        Self {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }
}

impl<R: Real> Div for Vector2<R> {
    type Output=Self;
    fn div(self, other: Self) -> Self {
        Self {
            x: self.x / other.x,
            y: self.y / other.y,
        }
    }
}

impl<R: Real> Mul<R> for Vector2<R> {
    type Output=Self;
    fn mul(self, other: R) -> Self {
        Self {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

impl<R: Real> Div<R> for Vector2<R> {
    type Output=Self;
    fn div(self, other: R) -> Self {
        Self {
            x: self.x / other,
            y: self.y / other,
        }
    }
}

#[derive(Clone, Copy)]
pub struct Bounds<R: Real> {
    pub top_right: Vector2<R>,
    pub bottom_left: Vector2<R>,
}

pub struct Manifold<R: Real> {
    pub num_contact_points: usize,
    pub penetration: R,
    pub normal: Vector2<R>,
    pub contact_points: [Vector2<R>; 2],
}

impl <R: Real> Default for Manifold<R> {
    fn default() -> Self {
        Self {
            num_contact_points: 0,
            penetration: R::ZERO,
            normal: Vector2::new(R::ZERO, R::ZERO),
            contact_points: [Vector2::new(R::ZERO, R::ZERO); 2],
        }
    }
}

pub fn point_to_plane_dist<R: Real>(p: Vector2<R>, n: Vector2<R>, dist: R) -> R {
    let n = n.normalized();
    let q = n * dist; // Plane's centre
    (p - q).dot(n)
}

pub fn closest_point_to_segment<R: Real>(a: Vector2<R>, b: Vector2<R>, p: Vector2<R>) -> Vector2<R> {
    let ab = b - a;
    let ap = p - a;
    let bp = p - b;

    if ab.dot(ap) <= R::ZERO {
        a
    } else if -ab.dot(bp) <= R::ZERO {
        b
    } else {
        let norm = ab.normalized();
        a + (norm * norm.dot(ap))
    }
}

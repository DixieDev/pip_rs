mod memory;
mod world;
mod circle;
mod capsule;
mod obb;

pub mod maths;
pub mod allocator;

pub use circle::{Circle, CircleConfig};
pub use capsule::{Capsule, CapsuleConfig};
pub use obb::{OrientedBox, OrientedBoxConfig};
pub use world::{World, WorldConfig};
pub use memory::{MemoryConfig, CircleHandle, CapsuleHandle, OrientedBoxHandle};

use maths::{Bounds, Manifold, Vector2};

pub trait Rigidbody<Real: maths::Real> : Clone {
    const TYPE: RigidbodyType;

    fn rb(&self) -> &RigidbodyData<Real>;
    fn rb_mut(&mut self) -> &mut RigidbodyData<Real>;

    fn intersect_bounds(&self, bounds: Bounds<Real>) -> bool;

    fn intersect_circle(&self, other: &Circle<Real>, manifold: &mut Manifold<Real>) -> bool;
    fn intersect_capsule(&self, other: &Capsule<Real>, manifold: &mut Manifold<Real>) -> bool;
    fn intersect_obb(&self, other: &OrientedBox<Real>, manifold: &mut Manifold<Real>) -> bool;

    fn sweep_circle(&self, other: &Circle<Real>, dt: Real, manifold: &mut Manifold<Real>) -> Real;
    fn sweep_capsule(&self, other: &Capsule<Real>, dt: Real, manifold: &mut Manifold<Real>) -> Real;
    fn sweep_obb(&self, other: &OrientedBox<Real>, dt: Real, manifold: &mut Manifold<Real>) -> Real;
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum RigidbodyType {
    Circle,
    Capsule,
    OBB,
}

#[derive(Clone)]
pub struct RigidbodyData<Real: maths::Real> {
    pub position: Vector2<Real>,
    pub prev_position: Vector2<Real>,
    pub rotation: Real,
    pub velocity: Vector2<Real>,
    pub angular_velocity: Real,
    pub acceleration: Vector2<Real>,
    pub mass: Real,
    pub restitution_coefficient: Real,
    pub time_in_sleep: Real,
    pub is_kinematic: bool,
    pub is_sleeping: bool,
    pub inertia: Real,
}


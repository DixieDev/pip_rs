use ggez::{Context, ContextBuilder, GameResult};
use ggez::event::{self, EventHandler};
use ggez::graphics;
use pip_rs::{World, WorldConfig, MemoryConfig, Circle, CircleConfig, CircleHandle, maths};
use pip_rs::allocator::DefaultAllocator;

const WIDTH: f32 = 800.0;
const HEIGHT: f32 = 600.0;

fn main() {
    let (mut ctx, mut event_loop) =
       ContextBuilder::new("PiP RS Test", "DixiE")
           .build()
           .unwrap();

    let mut my_game = MyGame::new(&mut ctx);

    match event::run(&mut ctx, &mut event_loop, &mut my_game) {
        Ok(_) => (),
        Err(e) => println!("Error occured: {}", e)
    }
}

struct MyGame {
    world: World<f32, DefaultAllocator>,
    circles: Vec<CircleHandle>,
}

impl MyGame {
    pub fn new(_ctx: &mut Context) -> MyGame {
        let mut world = World::new(WorldConfig {
            allocator: DefaultAllocator::new(1024 * 1024 * 128),
            memory_config: MemoryConfig {
                num_circles: 128,
                num_capsules: 128,
                num_boxes: 128,
            },
            step_mode: false,
            step_once: false,
            timestep: 0.016,
            max_accumulation: 1.0,
            gravity: 98.0,
            static_resolution: false,
            ignore_separating_bodies: true,
        }).unwrap() ;

        let mut circles = Vec::new();

        circles.push(world.create_circle(Circle::new(CircleConfig {
            radius: 20.0,
            position: maths::Vector2::new(WIDTH / 2.0, HEIGHT / 2.0),
            ..Default::default()
        })).unwrap());

        // Bottom line
        for i in 0..12 {
            circles.push(world.create_circle(Circle::new(CircleConfig {
                radius: 20.0,
                position: maths::Vector2::new(70.0 * (i as f32), 60.0),
                is_kinematic: true,
                ..Default::default()
            })).unwrap());
        }

        // Left line
        for i in 0..8 {
            circles.push(world.create_circle(Circle::new(CircleConfig {
                radius: 20.0,
                position: maths::Vector2::new(60.0, HEIGHT - 70.0 * (i as f32)),
                is_kinematic: true,
                ..Default::default()
            })).unwrap());
        }

        // Right line
        for i in 0..8 {
            circles.push(world.create_circle(Circle::new(CircleConfig {
                radius: 20.0,
                position: maths::Vector2::new(WIDTH - 60.0, HEIGHT - 70.0 * (i as f32)),
                is_kinematic: true,
                ..Default::default()
            })).unwrap());
        }

        MyGame {
            world,
            circles,
        }
    }
}

impl EventHandler for MyGame {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        let dt = ggez::timer::delta(ctx).as_secs_f32();
        self.world.update(dt);
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, graphics::BLACK);

        for circle in self.circles.iter().cloned() {
            let circle = self.world.get_circle(circle).unwrap();
            let mesh = ggez::graphics::MeshBuilder::new()
                .circle(
                    ggez::graphics::DrawMode::fill(),
                    [circle.rb.position.x, HEIGHT - circle.rb.position.y],
                    circle.radius,
                    0.5,
                    ggez::graphics::WHITE,
                )
                .build(ctx)?;

            graphics::draw(ctx, &mesh, ggez::graphics::DrawParam::default())?;
        }

        graphics::present(ctx)
    }
}
